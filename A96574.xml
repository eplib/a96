<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A96574">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>His Majesties speech to the States General, as he was seated in the chair, in their assembly</title>
    <title>Speeches. 1692</title>
    <author>England and Wales. Sovereign (1694-1702 : William III)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A96574 of text R230890 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing W2483A). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A96574</idno>
    <idno type="STC">Wing W2483A</idno>
    <idno type="STC">ESTC R230890</idno>
    <idno type="EEBO-CITATION">99896066</idno>
    <idno type="PROQUEST">99896066</idno>
    <idno type="VID">153760</idno>
    <idno type="PROQUESTGOID">2240908693</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A96574)</note>
    <note>Transcribed from: (Early English Books Online ; image set 153760)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 2377:17)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>His Majesties speech to the States General, as he was seated in the chair, in their assembly</title>
      <title>Speeches. 1692</title>
      <author>England and Wales. Sovereign (1694-1702 : William III)</author>
      <author>William III, King of England, 1650-1702.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>printed for Ed. Hawkins, in the Old. Baily,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1692.</date>
     </publicationStmt>
     <notesStmt>
      <note>Reproduction of original in the William Andrews Clark Memorial Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- William and Mary, 1689-1702 -- Early works to 1800.</term>
     <term>Great Britain -- Foreign relations -- Netherlands -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>His Majesties speech to the States General, as he was seated in the chair, in their assembly.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1692</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>416</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-06</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-06</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-07</date>
    <label>Robyn Anspach</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-07</date>
    <label>Robyn Anspach</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A96574-t">
  <body xml:id="A96574-e0">
   <div type="speech" xml:id="A96574-e10">
    <pb facs="tcp:153760:1" rend="simple:additions" xml:id="A96574-001-a"/>
    <head xml:id="A96574-e20">
     <w lemma="his" pos="po" xml:id="A96574-001-a-0010">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A96574-001-a-0020">Majesties</w>
     <w lemma="speech" pos="n1" xml:id="A96574-001-a-0030">SPEECH</w>
     <w lemma="to" pos="acp" xml:id="A96574-001-a-0040">TO</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-0050">THE</w>
     <w lemma="state" pos="ng1" reg="State's" xml:id="A96574-001-a-0060">States</w>
     <w lemma="general" pos="n1" xml:id="A96574-001-a-0070">General</w>
     <pc xml:id="A96574-001-a-0080">,</pc>
     <w lemma="as" pos="acp" xml:id="A96574-001-a-0090">As</w>
     <w lemma="he" pos="pns" xml:id="A96574-001-a-0100">He</w>
     <w lemma="be" pos="vvd" xml:id="A96574-001-a-0110">was</w>
     <w lemma="seat" pos="vvn" xml:id="A96574-001-a-0120">Seated</w>
     <w lemma="in" pos="acp" xml:id="A96574-001-a-0130">in</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-0140">the</w>
     <w lemma="chair" pos="n1" xml:id="A96574-001-a-0150">Chair</w>
     <pc xml:id="A96574-001-a-0160">,</pc>
     <w lemma="in" pos="acp" xml:id="A96574-001-a-0170">in</w>
     <w lemma="their" pos="po" xml:id="A96574-001-a-0180">Their</w>
     <w lemma="assembly" pos="n1" xml:id="A96574-001-a-0190">ASSEMBLY</w>
     <pc unit="sentence" xml:id="A96574-001-a-0200">.</pc>
    </head>
    <opener xml:id="A96574-e30">
     <salute xml:id="A96574-e40">
      <w lemma="my" pos="po" xml:id="A96574-001-a-0210">My</w>
      <w lemma="lord" pos="n2" xml:id="A96574-001-a-0220">Lords</w>
      <pc xml:id="A96574-001-a-0230">,</pc>
     </salute>
    </opener>
    <p xml:id="A96574-e50">
     <w lemma="upon" pos="acp" xml:id="A96574-001-a-0240">UPON</w>
     <w lemma="my" pos="po" xml:id="A96574-001-a-0250">my</w>
     <w lemma="last" pos="ord" xml:id="A96574-001-a-0260">last</w>
     <w lemma="departure" pos="n1" xml:id="A96574-001-a-0270">Departure</w>
     <w lemma="from" pos="acp" xml:id="A96574-001-a-0280">from</w>
     <w lemma="this" pos="d" xml:id="A96574-001-a-0290">this</w>
     <w lemma="country" pos="n1" xml:id="A96574-001-a-0300">Country</w>
     <pc xml:id="A96574-001-a-0310">,</pc>
     <w lemma="I" pos="pns" xml:id="A96574-001-a-0320">I</w>
     <w lemma="do" pos="vvd" xml:id="A96574-001-a-0330">did</w>
     <w lemma="intend" pos="vvi" xml:id="A96574-001-a-0340">intend</w>
     <w lemma="to" pos="prt" xml:id="A96574-001-a-0350">to</w>
     <w lemma="dispatch" pos="vvi" xml:id="A96574-001-a-0360">dispatch</w>
     <pc xml:id="A96574-001-a-0370">,</pc>
     <w lemma="with" pos="acp" xml:id="A96574-001-a-0380">with</w>
     <w lemma="what" pos="crq" xml:id="A96574-001-a-0390">what</w>
     <w lemma="expedition" pos="n1" xml:id="A96574-001-a-0400">Expedition</w>
     <w lemma="I" pos="pns" xml:id="A96574-001-a-0410">I</w>
     <w lemma="can" pos="vmd" xml:id="A96574-001-a-0420">could</w>
     <pc xml:id="A96574-001-a-0430">,</pc>
     <w lemma="the" pos="d" xml:id="A96574-001-a-0440">the</w>
     <w lemma="affair" pos="n2" xml:id="A96574-001-a-0450">Affairs</w>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-0460">of</w>
     <w lemma="my" pos="po" xml:id="A96574-001-a-0470">my</w>
     <w lemma="own" pos="d" xml:id="A96574-001-a-0480">own</w>
     <w lemma="kingdom" pos="n2" xml:id="A96574-001-a-0490">Kingdoms</w>
     <pc xml:id="A96574-001-a-0500">;</pc>
     <w lemma="which" pos="crq" xml:id="A96574-001-a-0510">which</w>
     <w lemma="have" pos="vvg" xml:id="A96574-001-a-0520">having</w>
     <w lemma="do" pos="vvn" xml:id="A96574-001-a-0530">done</w>
     <w lemma="according" pos="av-j" xml:id="A96574-001-a-0540">accordingly</w>
     <pc xml:id="A96574-001-a-0550">,</pc>
     <w lemma="I" pos="pns" xml:id="A96574-001-a-0560">I</w>
     <w lemma="be" pos="vvm" xml:id="A96574-001-a-0570">am</w>
     <w lemma="come" pos="vvn" xml:id="A96574-001-a-0580">come</w>
     <w lemma="hither" pos="av" xml:id="A96574-001-a-0590">hither</w>
     <w lemma="again" pos="av" xml:id="A96574-001-a-0600">again</w>
     <pc xml:id="A96574-001-a-0610">,</pc>
     <w lemma="to" pos="prt" xml:id="A96574-001-a-0620">to</w>
     <w lemma="execute" pos="vvb" xml:id="A96574-001-a-0630">Execute</w>
     <w lemma="my" pos="po" xml:id="A96574-001-a-0640">my</w>
     <w lemma="office" pos="n1" xml:id="A96574-001-a-0650">Office</w>
     <w lemma="as" pos="acp" xml:id="A96574-001-a-0660">as</w>
     <hi xml:id="A96574-e60">
      <w lemma="captain" pos="n1" xml:id="A96574-001-a-0670">Captain</w>
      <w lemma="general" pos="n1" xml:id="A96574-001-a-0680">General</w>
      <pc xml:id="A96574-001-a-0690">,</pc>
      <w lemma="admiral" pos="n1" xml:id="A96574-001-a-0700">Admiral</w>
      <pc xml:id="A96574-001-a-0710">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-0720">and</w>
     <hi xml:id="A96574-e70">
      <w lemma="statholder" pos="n1" xml:id="A96574-001-a-0730">Statholder</w>
      <pc xml:id="A96574-001-a-0740">;</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-0750">and</w>
     <w lemma="to" pos="prt" xml:id="A96574-001-a-0760">to</w>
     <w lemma="contribute" pos="vvi" xml:id="A96574-001-a-0770">Contribute</w>
     <w lemma="all" pos="d" xml:id="A96574-001-a-0780">all</w>
     <w lemma="that" pos="cs" xml:id="A96574-001-a-0790">that</w>
     <w lemma="be" pos="vvz" xml:id="A96574-001-a-0800">is</w>
     <w lemma="possible" pos="j" xml:id="A96574-001-a-0810">possible</w>
     <w lemma="for" pos="acp" xml:id="A96574-001-a-0820">for</w>
     <w lemma="i" pos="pno" xml:id="A96574-001-a-0830">Me</w>
     <w lemma="to" pos="prt" xml:id="A96574-001-a-0840">to</w>
     <w lemma="do" pos="vvi" xml:id="A96574-001-a-0850">do</w>
     <pc xml:id="A96574-001-a-0860">,</pc>
     <w lemma="for" pos="acp" xml:id="A96574-001-a-0870">for</w>
     <w lemma="a" pos="d" xml:id="A96574-001-a-0880">a</w>
     <w lemma="speedy" pos="j" xml:id="A96574-001-a-0890">speedy</w>
     <w lemma="peace" pos="n1" xml:id="A96574-001-a-0900">Peace</w>
     <pc xml:id="A96574-001-a-0910">;</pc>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-0920">and</w>
     <w lemma="to" pos="prt" xml:id="A96574-001-a-0930">to</w>
     <w lemma="put" pos="vvi" xml:id="A96574-001-a-0940">put</w>
     <w lemma="a" pos="d" xml:id="A96574-001-a-0950">an</w>
     <w lemma="end" pos="n1" xml:id="A96574-001-a-0960">End</w>
     <w lemma="to" pos="acp" xml:id="A96574-001-a-0970">to</w>
     <w lemma="this" pos="d" xml:id="A96574-001-a-0980">this</w>
     <w lemma="war-to" pos="j" xml:id="A96574-001-a-0990">War-To</w>
     <w lemma="which" pos="crq" xml:id="A96574-001-a-1000">which</w>
     <w lemma="end" pos="n1" xml:id="A96574-001-a-1010">End</w>
     <pc xml:id="A96574-001-a-1020">,</pc>
     <w lemma="I" pos="pns" xml:id="A96574-001-a-1030">I</w>
     <w lemma="have" pos="vvb" xml:id="A96574-001-a-1040">have</w>
     <w lemma="send" pos="vvn" xml:id="A96574-001-a-1050">sent</w>
     <w lemma="over" pos="acp" xml:id="A96574-001-a-1060">over</w>
     <w lemma="all" pos="d" xml:id="A96574-001-a-1070">all</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-1080">the</w>
     <w lemma="force" pos="n2" xml:id="A96574-001-a-1090">Forces</w>
     <pc xml:id="A96574-001-a-1100">,</pc>
     <w lemma="out" pos="av" xml:id="A96574-001-a-1110">out</w>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-1120">of</w>
     <w lemma="my" pos="po" xml:id="A96574-001-a-1130">my</w>
     <w lemma="kingdom" pos="n2" xml:id="A96574-001-a-1140">Kingdoms</w>
     <pc xml:id="A96574-001-a-1150">,</pc>
     <w lemma="that" pos="cs" xml:id="A96574-001-a-1160">that</w>
     <w lemma="I" pos="pns" xml:id="A96574-001-a-1170">I</w>
     <w lemma="can" pos="vmd" xml:id="A96574-001-a-1180">could</w>
     <w lemma="spare" pos="vvi" xml:id="A96574-001-a-1190">spare</w>
     <pc xml:id="A96574-001-a-1200">,</pc>
     <w lemma="to" pos="prt" xml:id="A96574-001-a-1210">to</w>
     <w lemma="join" pos="vvi" reg="join" xml:id="A96574-001-a-1220">joyn</w>
     <w lemma="those" pos="d" xml:id="A96574-001-a-1230">those</w>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-1240">of</w>
     <w lemma="this" pos="d" xml:id="A96574-001-a-1250">this</w>
     <hi xml:id="A96574-e80">
      <w lemma="State" pos="n1" xml:id="A96574-001-a-1260">State</w>
      <pc unit="sentence" xml:id="A96574-001-a-1261">.</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-1270">And</w>
     <w lemma="as" pos="acp" xml:id="A96574-001-a-1280">as</w>
     <w lemma="to" pos="acp" xml:id="A96574-001-a-1290">to</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-1300">the</w>
     <hi xml:id="A96574-e90">
      <w lemma="fleet" pos="n1" xml:id="A96574-001-a-1310">Fleet</w>
      <pc xml:id="A96574-001-a-1320">,</pc>
     </hi>
     <w lemma="I" pos="pns" xml:id="A96574-001-a-1330">I</w>
     <w lemma="have" pos="vvb" xml:id="A96574-001-a-1340">have</w>
     <w lemma="take" pos="vvn" xml:id="A96574-001-a-1350">taken</w>
     <w lemma="such" pos="d" xml:id="A96574-001-a-1360">such</w>
     <w lemma="care" pos="n1" xml:id="A96574-001-a-1370">Care</w>
     <pc xml:id="A96574-001-a-1380">,</pc>
     <w lemma="that" pos="cs" xml:id="A96574-001-a-1390">that</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-1400">the</w>
     <w lemma="most" pos="avs-d" xml:id="A96574-001-a-1410">most</w>
     <w lemma="part" pos="n1" xml:id="A96574-001-a-1420">part</w>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-1430">of</w>
     <w lemma="it" pos="pn" xml:id="A96574-001-a-1440">it</w>
     <w lemma="be" pos="vvz" xml:id="A96574-001-a-1450">is</w>
     <w lemma="now" pos="av" xml:id="A96574-001-a-1460">now</w>
     <w lemma="ready" pos="j" xml:id="A96574-001-a-1470">ready</w>
     <pc xml:id="A96574-001-a-1480">;</pc>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-1490">and</w>
     <w lemma="hope" pos="vvb" xml:id="A96574-001-a-1500">hope</w>
     <pc xml:id="A96574-001-a-1510">,</pc>
     <w lemma="that" pos="cs" xml:id="A96574-001-a-1520">That</w>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-1530">of</w>
     <w lemma="this" pos="d" xml:id="A96574-001-a-1540">this</w>
     <w lemma="state" pos="n1" xml:id="A96574-001-a-1550">State</w>
     <w lemma="be" pos="vvz" xml:id="A96574-001-a-1560">is</w>
     <w lemma="in" pos="acp" xml:id="A96574-001-a-1570">in</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-1580">the</w>
     <w lemma="like" pos="j" xml:id="A96574-001-a-1590">like</w>
     <w lemma="posture" pos="n1" xml:id="A96574-001-a-1600">Posture</w>
     <pc xml:id="A96574-001-a-1610">:</pc>
     <w lemma="desire" pos="vvg" xml:id="A96574-001-a-1620">desiring</w>
     <pc xml:id="A96574-001-a-1630">,</pc>
     <w lemma="that" pos="cs" xml:id="A96574-001-a-1640">that</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-1650">the</w>
     <hi xml:id="A96574-e100">
      <w lemma="great" pos="j" xml:id="A96574-001-a-1660">Great</w>
      <w lemma="God" pos="nn1" xml:id="A96574-001-a-1670">God</w>
      <pc xml:id="A96574-001-a-1680">,</pc>
     </hi>
     <w lemma="the" pos="d" xml:id="A96574-001-a-1690">the</w>
     <w lemma="maker" pos="n1" xml:id="A96574-001-a-1700">Maker</w>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-1710">of</w>
     <w lemma="all" pos="d" xml:id="A96574-001-a-1720">all</w>
     <w lemma="thing" pos="n2" xml:id="A96574-001-a-1730">things</w>
     <pc xml:id="A96574-001-a-1740">,</pc>
     <w lemma="will" pos="vmd" xml:id="A96574-001-a-1750">would</w>
     <w lemma="so" pos="av" xml:id="A96574-001-a-1760">so</w>
     <w lemma="bless" pos="vvb" xml:id="A96574-001-a-1770">Bless</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-1780">the</w>
     <w lemma="arm" pos="n2" xml:id="A96574-001-a-1790">Arms</w>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-1800">of</w>
     <w lemma="this" pos="d" xml:id="A96574-001-a-1810">this</w>
     <hi xml:id="A96574-e110">
      <w lemma="state" pos="n1" xml:id="A96574-001-a-1820">State</w>
      <pc xml:id="A96574-001-a-1830">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-1840">and</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-1850">the</w>
     <w lemma="rest" pos="n1" xml:id="A96574-001-a-1860">rest</w>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-1870">of</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-1880">the</w>
     <hi xml:id="A96574-e120">
      <w lemma="confederate" pos="n2-j" xml:id="A96574-001-a-1890">Confederates</w>
      <pc xml:id="A96574-001-a-1900">,</pc>
     </hi>
     <w lemma="that" pos="cs" xml:id="A96574-001-a-1910">that</w>
     <w lemma="they" pos="pns" xml:id="A96574-001-a-1920">they</w>
     <w lemma="may" pos="vmb" xml:id="A96574-001-a-1930">may</w>
     <w lemma="obtain" pos="vvi" xml:id="A96574-001-a-1940">obtain</w>
     <w lemma="that" pos="cs" xml:id="A96574-001-a-1950">that</w>
     <w lemma="which" pos="crq" xml:id="A96574-001-a-1960">which</w>
     <w lemma="be" pos="vvz" xml:id="A96574-001-a-1970">is</w>
     <w lemma="most" pos="avs-d" xml:id="A96574-001-a-1980">most</w>
     <w lemma="in" pos="acp" xml:id="A96574-001-a-1990">in</w>
     <w lemma="their" pos="po" xml:id="A96574-001-a-2000">their</w>
     <w lemma="eye" pos="n1" xml:id="A96574-001-a-2010">Eye</w>
     <pc xml:id="A96574-001-a-2020">,</pc>
     <w lemma="a" pos="d" xml:id="A96574-001-a-2030">A</w>
     <hi xml:id="A96574-e130">
      <w lemma="good" pos="j" xml:id="A96574-001-a-2040">Good</w>
      <w lemma="peace" pos="n1" xml:id="A96574-001-a-2050">Peace</w>
      <pc unit="sentence" xml:id="A96574-001-a-2060">.</pc>
     </hi>
     <w lemma="I" pos="pns" xml:id="A96574-001-a-2070">I</w>
     <w lemma="will" pos="vmb" xml:id="A96574-001-a-2080">will</w>
     <w lemma="do" pos="vvi" xml:id="A96574-001-a-2090">do</w>
     <w lemma="all" pos="d" xml:id="A96574-001-a-2100">all</w>
     <w lemma="that" pos="cs" xml:id="A96574-001-a-2110">that</w>
     <w lemma="I" pos="pns" xml:id="A96574-001-a-2120">I</w>
     <w lemma="can" pos="vmb" xml:id="A96574-001-a-2130">can</w>
     <pc xml:id="A96574-001-a-2140">,</pc>
     <w lemma="for" pos="acp" xml:id="A96574-001-a-2150">for</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-2160">the</w>
     <w lemma="common" pos="j" xml:id="A96574-001-a-2170">Common</w>
     <w lemma="good" pos="j" xml:id="A96574-001-a-2180">Good</w>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-2190">and</w>
     <w lemma="welfare" pos="n1" xml:id="A96574-001-a-2200">Welfare</w>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-2210">of</w>
     <w lemma="this" pos="d" xml:id="A96574-001-a-2220">this</w>
     <hi xml:id="A96574-e140">
      <w lemma="state" pos="n1" xml:id="A96574-001-a-2230">State</w>
      <pc xml:id="A96574-001-a-2240">,</pc>
     </hi>
     <w lemma="not" pos="xx" xml:id="A96574-001-a-2250">not</w>
     <w lemma="spare" pos="vvg" xml:id="A96574-001-a-2260">sparing</w>
     <w lemma="my" pos="po" xml:id="A96574-001-a-2270">my</w>
     <w lemma="own" pos="d" xml:id="A96574-001-a-2280">own</w>
     <w lemma="person" pos="n1" xml:id="A96574-001-a-2290">Person</w>
     <pc xml:id="A96574-001-a-2300">;</pc>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-2310">and</w>
     <w lemma="I" pos="pns" xml:id="A96574-001-a-2320">I</w>
     <w lemma="assure" pos="vvb" xml:id="A96574-001-a-2330">assure</w>
     <w lemma="you" pos="pn" xml:id="A96574-001-a-2340">you</w>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-2350">of</w>
     <w lemma="my" pos="po" xml:id="A96574-001-a-2360">my</w>
     <w lemma="sincere" pos="j" xml:id="A96574-001-a-2370">Sincere</w>
     <w lemma="intention" pos="n2" xml:id="A96574-001-a-2380">Intentions</w>
     <pc xml:id="A96574-001-a-2390">,</pc>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-2400">and</w>
     <w lemma="good" pos="j" xml:id="A96574-001-a-2410">good</w>
     <w lemma="affection" pos="n1" xml:id="A96574-001-a-2420">Affection</w>
     <w lemma="to" pos="acp" xml:id="A96574-001-a-2430">to</w>
     <w lemma="you" pos="pn" xml:id="A96574-001-a-2440">You</w>
     <pc xml:id="A96574-001-a-2450">;</pc>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-2460">and</w>
     <w lemma="thank" pos="vvb" xml:id="A96574-001-a-2470">thank</w>
     <w lemma="your" pos="po" xml:id="A96574-001-a-2480">your</w>
     <w lemma="lordship" pos="n2" rend="hi" xml:id="A96574-001-a-2490">Lordships</w>
     <w lemma="for" pos="acp" xml:id="A96574-001-a-2500">for</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-2510">the</w>
     <w lemma="honour" pos="n1" xml:id="A96574-001-a-2520">Honour</w>
     <w lemma="you" pos="pn" xml:id="A96574-001-a-2530">you</w>
     <w lemma="have" pos="vvb" xml:id="A96574-001-a-2540">have</w>
     <w lemma="do" pos="vvn" xml:id="A96574-001-a-2550">done</w>
     <w lemma="i" pos="pno" xml:id="A96574-001-a-2560">Me</w>
     <pc xml:id="A96574-001-a-2570">,</pc>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-2580">and</w>
     <w lemma="for" pos="acp" xml:id="A96574-001-a-2590">for</w>
     <w lemma="your" pos="po" xml:id="A96574-001-a-2600">your</w>
     <w lemma="congratulation" pos="n2" xml:id="A96574-001-a-2610">Congratulations</w>
     <w lemma="upon" pos="acp" xml:id="A96574-001-a-2620">upon</w>
     <w lemma="my" pos="po" xml:id="A96574-001-a-2630">my</w>
     <w lemma="arrival" pos="n1" xml:id="A96574-001-a-2640">Arrival</w>
     <pc unit="sentence" xml:id="A96574-001-a-2650">.</pc>
    </p>
   </div>
   <div type="response" xml:id="A96574-e160">
    <head xml:id="A96574-e170">
     <w lemma="the" pos="d" xml:id="A96574-001-a-2660">The</w>
     <w lemma="answer" pos="n1" xml:id="A96574-001-a-2670">Answer</w>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-2680">of</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-2690">the</w>
     <w lemma="state" pos="ng1" reg="State's" xml:id="A96574-001-a-2700">States</w>
     <w lemma="general" pos="n1" xml:id="A96574-001-a-2710">General</w>
     <pc xml:id="A96574-001-a-2720">,</pc>
     <w lemma="to" pos="prt" xml:id="A96574-001-a-2730">to</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-2740">the</w>
     <w lemma="king" pos="ng1" reg="King's" xml:id="A96574-001-a-2750">Kings</w>
     <w lemma="speech" pos="n1" xml:id="A96574-001-a-2760">SPEECH</w>
     <pc unit="sentence" xml:id="A96574-001-a-2770">.</pc>
    </head>
    <opener xml:id="A96574-e180">
     <salute xml:id="A96574-e190">
      <w lemma="most" pos="ds" xml:id="A96574-001-a-2780">Most</w>
      <w lemma="sovereign" pos="j" xml:id="A96574-001-a-2790">Sovereign</w>
      <w lemma="prince" pos="n1" xml:id="A96574-001-a-2800">Prince</w>
      <pc xml:id="A96574-001-a-2810">,</pc>
     </salute>
    </opener>
    <p xml:id="A96574-e200">
     <w lemma="we" pos="pns" xml:id="A96574-001-a-2820">WE</w>
     <w lemma="thank" pos="vvb" xml:id="A96574-001-a-2830">thank</w>
     <w lemma="your" pos="po" xml:id="A96574-001-a-2840">Your</w>
     <w lemma="majesty" pos="n1" xml:id="A96574-001-a-2850">Majesty</w>
     <w lemma="for" pos="acp" xml:id="A96574-001-a-2860">for</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-2870">the</w>
     <w lemma="honour" pos="n1" xml:id="A96574-001-a-2880">Honour</w>
     <w lemma="you" pos="pn" xml:id="A96574-001-a-2890">You</w>
     <w lemma="have" pos="vvb" xml:id="A96574-001-a-2900">have</w>
     <w lemma="do" pos="vvn" xml:id="A96574-001-a-2910">done</w>
     <w lemma="we" pos="pno" reg="Us" xml:id="A96574-001-a-2920">Vs</w>
     <pc xml:id="A96574-001-a-2930">,</pc>
     <w lemma="to" pos="prt" xml:id="A96574-001-a-2940">to</w>
     <w lemma="appear" pos="vvi" xml:id="A96574-001-a-2950">appear</w>
     <w lemma="in" pos="acp" xml:id="A96574-001-a-2960">in</w>
     <w lemma="our" pos="po" xml:id="A96574-001-a-2970">Our</w>
     <w lemma="assembly" pos="n1" xml:id="A96574-001-a-2980">Assembly</w>
     <pc xml:id="A96574-001-a-2990">;</pc>
     <w lemma="as" pos="acp" xml:id="A96574-001-a-3000">as</w>
     <w lemma="also" pos="av" xml:id="A96574-001-a-3010">also</w>
     <pc xml:id="A96574-001-a-3020">,</pc>
     <w lemma="the" pos="d" xml:id="A96574-001-a-3030">the</w>
     <w lemma="constant" pos="j" xml:id="A96574-001-a-3040">constant</w>
     <w lemma="care" pos="n1" xml:id="A96574-001-a-3050">Care</w>
     <pc xml:id="A96574-001-a-3060">,</pc>
     <w lemma="good" pos="j" xml:id="A96574-001-a-3070">Good</w>
     <w lemma="Will" pos="nn1" xml:id="A96574-001-a-3080">Will</w>
     <pc xml:id="A96574-001-a-3090">,</pc>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-3100">and</w>
     <w lemma="affection" pos="n1" xml:id="A96574-001-a-3110">Affection</w>
     <pc xml:id="A96574-001-a-3120">,</pc>
     <w lemma="you" pos="pn" xml:id="A96574-001-a-3130">You</w>
     <w lemma="have" pos="vvb" xml:id="A96574-001-a-3140">have</w>
     <w lemma="always" pos="av" xml:id="A96574-001-a-3150">always</w>
     <w lemma="manifest" pos="vvn" xml:id="A96574-001-a-3160">manifested</w>
     <w lemma="towards" pos="acp" xml:id="A96574-001-a-3170">towards</w>
     <w lemma="we" pos="pno" reg="Us" xml:id="A96574-001-a-3180">Vs</w>
     <pc xml:id="A96574-001-a-3190">,</pc>
     <w lemma="in" pos="acp" xml:id="A96574-001-a-3200">in</w>
     <w lemma="promote" pos="j-vg" xml:id="A96574-001-a-3210">Promoting</w>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-3220">of</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-3230">the</w>
     <w lemma="common" pos="j" xml:id="A96574-001-a-3240">common</w>
     <w lemma="welfare" pos="n1" xml:id="A96574-001-a-3250">Welfare</w>
     <pc xml:id="A96574-001-a-3260">;</pc>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-3270">of</w>
     <w lemma="which" pos="crq" xml:id="A96574-001-a-3280">which</w>
     <w lemma="your" pos="po" xml:id="A96574-001-a-3290">your</w>
     <w lemma="majesty" pos="n1" xml:id="A96574-001-a-3300">Majesty</w>
     <w lemma="have" pos="vvz" xml:id="A96574-001-a-3310">has</w>
     <w lemma="give" pos="vvn" xml:id="A96574-001-a-3320">given</w>
     <w lemma="we" pos="pno" reg="Us" xml:id="A96574-001-a-3330">Vs</w>
     <w lemma="fresh" pos="j" xml:id="A96574-001-a-3340">fresh</w>
     <w lemma="proof" pos="n2" xml:id="A96574-001-a-3350">Proofs</w>
     <pc xml:id="A96574-001-a-3360">,</pc>
     <w lemma="in" pos="acp" xml:id="A96574-001-a-3370">in</w>
     <w lemma="come" pos="vvg" xml:id="A96574-001-a-3380">coming</w>
     <w lemma="to" pos="acp" xml:id="A96574-001-a-3390">to</w>
     <w lemma="we" pos="pno" reg="Us" xml:id="A96574-001-a-3400">Vs</w>
     <w lemma="at" pos="acp" xml:id="A96574-001-a-3410">at</w>
     <w lemma="such" pos="d" xml:id="A96574-001-a-3420">such</w>
     <w lemma="a" pos="d" xml:id="A96574-001-a-3430">a</w>
     <w lemma="season" pos="n1" xml:id="A96574-001-a-3440">Season</w>
     <w lemma="of" pos="acp" xml:id="A96574-001-a-3450">of</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-3460">the</w>
     <w lemma="year" pos="n1" xml:id="A96574-001-a-3470">Year</w>
     <pc unit="sentence" xml:id="A96574-001-a-3480">.</pc>
     <w lemma="we" pos="pns" xml:id="A96574-001-a-3490">We</w>
     <w lemma="assure" pos="vvb" xml:id="A96574-001-a-3500">assure</w>
     <w lemma="your" pos="po" xml:id="A96574-001-a-3510">Your</w>
     <w lemma="majesty" pos="n1" xml:id="A96574-001-a-3520">Majesty</w>
     <pc xml:id="A96574-001-a-3530">,</pc>
     <w lemma="that" pos="cs" xml:id="A96574-001-a-3540">That</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-3550">the</w>
     <w lemma="favour" pos="n1" xml:id="A96574-001-a-3560">Favour</w>
     <w lemma="which" pos="crq" xml:id="A96574-001-a-3570">which</w>
     <w lemma="you" pos="pn" xml:id="A96574-001-a-3580">You</w>
     <w lemma="have" pos="vvb" xml:id="A96574-001-a-3590">have</w>
     <w lemma="always" pos="av" xml:id="A96574-001-a-3600">always</w>
     <w lemma="show" pos="vvn" xml:id="A96574-001-a-3610">shown</w>
     <pc xml:id="A96574-001-a-3620">,</pc>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-3630">and</w>
     <w lemma="daily" pos="av-j" xml:id="A96574-001-a-3640">daily</w>
     <w lemma="continue" pos="vvz" xml:id="A96574-001-a-3650">Continues</w>
     <pc xml:id="A96574-001-a-3660">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A96574-001-a-3670">shall</w>
     <w lemma="be" pos="vvi" xml:id="A96574-001-a-3680">be</w>
     <w lemma="for" pos="acp" xml:id="A96574-001-a-3690">for</w>
     <w lemma="ever" pos="av" xml:id="A96574-001-a-3700">ever</w>
     <w lemma="own" pos="vvn" xml:id="A96574-001-a-3710">owned</w>
     <w lemma="by" pos="acp" xml:id="A96574-001-a-3720">by</w>
     <w lemma="we" pos="pno" reg="Us" xml:id="A96574-001-a-3730">Vs</w>
     <pc xml:id="A96574-001-a-3740">,</pc>
     <w lemma="with" pos="acp" xml:id="A96574-001-a-3750">with</w>
     <w lemma="great" pos="j" xml:id="A96574-001-a-3760">great</w>
     <w lemma="thankfulness" pos="n1" xml:id="A96574-001-a-3770">Thankfulness</w>
     <pc xml:id="A96574-001-a-3780">;</pc>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-3790">and</w>
     <w lemma="that" pos="cs" xml:id="A96574-001-a-3800">that</w>
     <w lemma="as" pos="acp" xml:id="A96574-001-a-3810">as</w>
     <w lemma="much" pos="av-d" xml:id="A96574-001-a-3820">much</w>
     <w lemma="as" pos="acp" xml:id="A96574-001-a-3830">as</w>
     <w lemma="in" pos="acp" xml:id="A96574-001-a-3840">in</w>
     <w lemma="we" pos="pno" reg="Us" xml:id="A96574-001-a-3850">Vs</w>
     <w lemma="lie" pos="vvz" xml:id="A96574-001-a-3860">lies</w>
     <pc xml:id="A96574-001-a-3870">,</pc>
     <w lemma="we" pos="pns" xml:id="A96574-001-a-3880">We</w>
     <w lemma="will" pos="vmb" xml:id="A96574-001-a-3890">will</w>
     <w lemma="second" pos="vvi" xml:id="A96574-001-a-3900">Second</w>
     <w lemma="your" pos="po" xml:id="A96574-001-a-3910">your</w>
     <w lemma="majesty" pos="n1" xml:id="A96574-001-a-3920">Majesty</w>
     <pc xml:id="A96574-001-a-3930">,</pc>
     <w lemma="in" pos="acp" xml:id="A96574-001-a-3940">in</w>
     <w lemma="your" pos="po" xml:id="A96574-001-a-3950">Your</w>
     <w lemma="good" pos="j" xml:id="A96574-001-a-3960">Good</w>
     <w lemma="intention" pos="n2" xml:id="A96574-001-a-3970">Intentions</w>
     <w lemma="and" pos="cc" xml:id="A96574-001-a-3980">and</w>
     <w lemma="design" pos="n2" xml:id="A96574-001-a-3990">Designs</w>
     <pc unit="sentence" xml:id="A96574-001-a-4000">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A96574-e210">
   <div type="colophon" xml:id="A96574-e220">
    <p xml:id="A96574-e230">
     <hi xml:id="A96574-e240">
      <w lemma="London" pos="nn1" xml:id="A96574-001-a-4010">London</w>
      <pc xml:id="A96574-001-a-4020">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A96574-001-a-4030">Printed</w>
     <w lemma="for" pos="acp" xml:id="A96574-001-a-4040">for</w>
     <hi xml:id="A96574-e250">
      <w lemma="ed." pos="ab" xml:id="A96574-001-a-4050">Ed.</w>
      <w lemma="Hawkins" pos="nn1" xml:id="A96574-001-a-4060">Hawkins</w>
      <pc xml:id="A96574-001-a-4070">,</pc>
     </hi>
     <w lemma="in" pos="acp" xml:id="A96574-001-a-4080">in</w>
     <w lemma="the" pos="d" xml:id="A96574-001-a-4090">the</w>
     <hi xml:id="A96574-e260">
      <w lemma="Old" pos="j" xml:id="A96574-001-a-4100">Old</w>
      <pc unit="sentence" xml:id="A96574-001-a-4101">.</pc>
      <w lemma="Baily" pos="nn1" xml:id="A96574-001-a-4110">Baily</w>
      <pc xml:id="A96574-001-a-4120">,</pc>
     </hi>
     <w lemma="1692." pos="crd" xml:id="A96574-001-a-4130">1692.</w>
     <pc unit="sentence" xml:id="A96574-001-a-4140"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
